{
  description = "Tricot, a reverse proxy with consul integration";

  # Nixpkgs 24.11 as of 2025-01-12
  inputs.nixpkgs.url =
    "github:NixOS/nixpkgs/7c4869c47090dd7f9f1bdfb49a22aea026996815";

  # Rust overlay as of 2025-02-03
  inputs.rust-overlay.url =
    "github:oxalica/rust-overlay/35c6f8c4352f995ecd53896200769f80a3e8f22d";
  inputs.rust-overlay.inputs.nixpkgs.follows = "nixpkgs";

  inputs.crane.url = "github:ipetkov/crane";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, crane, flake-utils, rust-overlay, ... }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (localSystem:
      let
        crossSystem = "x86_64-unknown-linux-musl";
        rustVersion = "1.84.0";

        pkgs = import nixpkgs {
          # we need a full musl toolchain because we're depending on openssl (a
          # C library), which we thus need to compile for musl
          inherit localSystem;
          crossSystem = { config = crossSystem; };
          overlays = [ (import rust-overlay) ];
        };

        craneLib = (crane.mkLib pkgs).overrideToolchain
          (p: p.rust-bin.stable.${rustVersion}.default);

        commonArgs = {
          src = craneLib.cleanCargoSource ./.;
          strictDeps = true;

          # required for building openssl
          nativeBuildInputs = [
            pkgs.perl
          ];
          OPENSSL_STATIC = "yes";

          CARGO_BUILD_TARGET = crossSystem;
          CARGO_BUILD_RUSTFLAGS = "-C target-feature=+crt-static";

          doCheck = false;
        };

        devArgs = (commonArgs // { CARGO_PROFILE = "dev"; });
        releaseArgs = (commonArgs // { CARGO_PROFILE = "release"; });

        tricot-deps-dev = craneLib.buildDepsOnly devArgs;
      in
        {
          packages = rec {
            default = craneLib.buildPackage releaseArgs;

            debug = craneLib.buildPackage (devArgs // {
              cargoArtifacts = tricot-deps-dev;
            });

            test = craneLib.cargoTest (devArgs // {
              cargoArtifacts = tricot-deps-dev;
              doCheck = true;
            });

            docker = pkgs.dockerTools.buildImage {
              name = "tricot";
              config = {
                contents = [ pkgs.cacert ];
                Cmd = [ "${self.packages.x86_64-linux.default}/bin/tricot" ];
              };
            };
          };
        }
    );
}
